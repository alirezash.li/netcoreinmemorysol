#pragma checksum "D:\netcoreinmemorysol\webAppTest\WepApp\Views\Home\Building.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "84f3cc93c7d882ed442ef0bffd01c6d445502b6b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Building), @"mvc.1.0.view", @"/Views/Home/Building.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"84f3cc93c7d882ed442ef0bffd01c6d445502b6b", @"/Views/Home/Building.cshtml")]
    public class Views_Home_Building : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Controller/Building.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "D:\netcoreinmemorysol\webAppTest\WepApp\Views\Home\Building.cshtml"
  
    ViewData["Title"] = "Building";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Building</h1>

<div ng-app=""mainApp"" ng-controller=""buildingController"">
    <form name=""frm"">
        <table class=""table"" name=""tbt"">
            <thead>
                <tr>
                    <td>id </td>
                    <td>buildingName</td>
                    <td>direction</td>
                    <td>meter</td>
                    <td>number </td>
                    <td>address </td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat=""x in buildings"">
                    <td>{{ x.id }}</td>
                    <td>{{ x.buildingName }}</td>
                    <td>{{ x.direction }}</td>
                    <td>{{ x.meter }}</td>
                    <td>{{ x.number }}</td>
                    <td>{{ x.address }}</td>
                    <td>
                        <input type=""button"" class=""btn-primary"" value=""Delete"" ng-click="" delete(x)""/>
                    </td>
                   
    ");
            WriteLiteral(@"            </tr>
                <tr>

                    <td></td>
                    <td>
                        <input type=""text"" id=""buildingName"" name=""buildingName"" ng-model=""newObj.buildingName"" ng-required=""true"" />
                        <span ng-if=""!frm.buildingName.$valid"" class=""text-warning"">buildingName is required</span>
                    </td>
                    <td>
");
            WriteLiteral(@"                        <select class=""form-control "" style=""width:150px"" ng-model=""model.direction"" ng-options=""o as o.name for o in directionList""></select>

                    </td>
                    <td>
                        <input type=""number"" id=""meter"" name=""meter"" ng-model=""newObj.meter"" required/>
                        <span ng-if=""!frm.meter.$valid"" class=""text-warning"">meter is required</span>
                    </td>
                    <td>
                        <input type=""text"" id=""number"" name=""number"" ng-model=""newObj.number"" required/>
                        <span ng-if=""!frm.number.$valid"" class=""text-warning"">number is required</span>
                    </td>
                    <td>
                        <input type=""text"" id=""address"" name=""address"" ng-model=""newObj.address"" required/>
                        <span ng-if=""!frm.address.$valid"" class=""text-warning"">address is required</span>
                    </td>
                    <td>
               ");
            WriteLiteral("         <input type=\"button\" class=\"btn-primary\" value=\"Add\" ng-click=\"SaveBuilding()\" ng-disabled=\"!frm.$valid\"  />\r\n                    </td>\r\n\r\n                </tr>\r\n            </tbody>\r\n        </table>\r\n    </form>\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "84f3cc93c7d882ed442ef0bffd01c6d445502b6b6130", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
