﻿app.controller('buildingController', function ($scope, $http, $filter) {
    $scope.newObj = {};
    $scope.directionList = {};
    $scope.model = {};
    $scope.GetBuilding = function () {
        $http.get("/api/GetAllBuilding")
            .then(function (response) {
                $scope.buildings = response.data;
            });
    }
    $scope.GetDirection = function () {
        $http.get("/api/GetAllDirection")
            .then(function (response) {
                $scope.directionList = response.data;
                $scope.model.direction = $scope.directionList[0];
            });
    }
    $scope.SaveBuilding = function (obj) {
        $scope.newObj.direction = $scope.model.direction.id;
        $http.post("/api/SaveBuilding", $scope.newObj)
            .then(function (response) {
                $scope.GetBuilding();
                $scope.newObj = {};
            });
    }
    $scope.delete = function (obj) {
        $http.post("/api/DeleteBuilding", obj)
            .then(function (response) {
                $scope.GetBuilding();
            });
    }
    $scope.init = function () {
        $scope.GetDirection();
        $scope.GetBuilding();
    }
    $scope.init()
});