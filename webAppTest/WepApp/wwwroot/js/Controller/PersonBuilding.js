﻿app.controller('buildingController', function ($scope, $http, $filter) {
    $scope.newObj = {};
    $scope.model = {};
    $scope.personList = {};
    $scope.buildingList = {};
    $scope.GetPersonBuilding = function () {
        $http.get("/api/GetAllPersonBuilding")
            .then(function (response) {
                $scope.pbs = response.data;
            });
    }
    $scope.GetPersons = function () {
        $http.get("/api/GetAllPerson")
            .then(function (response) {
                $scope.personList = response.data;
                $scope.model.person = $scope.personList[0];
            });
    }
    $scope.GetBuilding = function () {
        $http.get("/api/GetAllBuilding")
            .then(function (response) {
                $scope.buildingList = response.data;
                $scope.model.building = $scope.buildingList[0];
            });
    }
    $scope.SavePersonBuilding = function (obj) {
        $scope.newObj.personid = $scope.model.person.id;
        $scope.newObj.buildingId = $scope.model.building.id;
        $http.post("/api/SavePersonBuilding", $scope.newObj)
            .then(function (response) {
                $scope.GetPersonBuilding();
                $scope.newObj = {};
            });
    }
    $scope.delete = function (obj) {
        $http.post("/api/DeletePersonBuilding", obj)
            .then(function (response) {
                $scope.GetPersonBuilding();
            });
    }
    $scope.init = function () {
        $scope.GetPersonBuilding();
        $scope.GetPersons();
        $scope.GetBuilding();
    }
    $scope.init()
});