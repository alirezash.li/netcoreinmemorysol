﻿using DataAccess;
using Models.DBModel;
using Models.Enums;
using System.Collections.Generic;

namespace WepApp.Common
{
    public class DataInit
    {
        public static void SeadData(MContext dbcontext)
        {
            dbcontext.InMemoryInit(new List<BuildingModel>() {
                new BuildingModel() { id = 1, address = "address1",
                    buildingName = "buildingName",
                    direction = DirectionEnum.Notrth,
                    meter = 1,
                    number = "number1" ,
                    IsActive=true
                }
            }, new List<PersonModel>() {
                    new PersonModel() {
                        firstName = "userf1",
                        lastName = "userl1",
                        TelNumber = "09359653223",
                        id = 1
                    },
                    new PersonModel() {
                        firstName = "userf2",
                        lastName = "userl2",
                        TelNumber = "09359653223",
                        id = 2
                    }
            });
        }
    }
}
