﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using Models.DBModel;
using Models.Enums;
using System;
using System.Linq;

namespace WepApp.Controllers
{
    public class ApiController : Controller
    {

        private readonly MContext _mContext;
        public ApiController(MContext mContext)
        {
            _mContext = mContext;
        }
        [HttpGet]
        public IActionResult GetAllBuilding()
        {
            return Ok(_mContext.bulidingService.GetBuildings());
        }
        [HttpGet]
        public IActionResult GetAllPerson()
        {
            return Ok(_mContext.personService.GetPerosns());
        }
        [HttpGet]
        public IActionResult GetAllDirection()
        {
            var directionList = Enum.GetValues(typeof(DirectionEnum)).Cast<int>().Select(p => new { id = p, name = ((DirectionEnum)p).ToString() }).ToList();
            return Ok(directionList);
        }
        [HttpGet]
        public IActionResult GetAllPersonBuilding()
        {
            var tmps = _mContext.PersonBuildingService.GetPersonBuildings();
            return Ok(tmps);
        }

        [HttpPost]
        public IActionResult SaveBuilding([FromBody]BuildingModel input)
        {
            return Ok(_mContext.bulidingService.SaveBuilding(input));
        }
        [HttpPost]
        public IActionResult DeleteBuilding([FromBody]BuildingModel input)
        {
            return Ok(_mContext.bulidingService.DeleteBuilding(input));
        }

        [HttpPost]
        public IActionResult SavePersonBuilding([FromBody]PersonBuildingModel input)
        {
            return Ok(_mContext.PersonBuildingService.SavePersonBuildings(input));
        }
        [HttpPost]
        public IActionResult DeletePersonBuilding([FromBody]PersonBuildingModel input)
        {
            return Ok(_mContext.PersonBuildingService.DeletePersonBuildings(input));
        }
    }
}