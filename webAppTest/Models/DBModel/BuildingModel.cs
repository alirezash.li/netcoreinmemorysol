﻿using Models.ArcModel;
using Models.Enums;

namespace Models.DBModel
{
    public class BuildingModel : ParentCls
    {
        public string number { get; set; }
        public string buildingName { get; set; }
        public decimal meter { get; set; }
        public string address { get; set; }
        public bool IsActive { get; set; }
        public DirectionEnum direction { get; set; }

    }
}
