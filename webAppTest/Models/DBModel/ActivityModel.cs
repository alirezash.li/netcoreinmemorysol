﻿using Models.ArcModel;
using System;

namespace Models.DBModel
{
    public class ActivityModel : ParentCls
    {
        public DateTime createDate { get; set; }
        public string tableName { get; set; }
        public long tableId { get; set; }
        public string action { get; set; }
    }
}
