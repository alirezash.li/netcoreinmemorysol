﻿using Models.ArcModel;

namespace Models.DBModel
{
    public class PersonModel : ParentCls
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string TelNumber { get; set; }



        public virtual string GetFullName => firstName + lastName;
    }
}
