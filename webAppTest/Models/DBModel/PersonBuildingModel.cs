﻿using Models.ArcModel;
using System;

namespace Models.DBModel
{
    public class PersonBuildingModel : ParentCls
    {
        public long personid { get; set; }
        public PersonModel person { get; set; }
        public bool isActive { get; set; }
        public long buildingId { get; set; }

        public BuildingModel building { get; set; }

        public DateTime createDate { get; set; }
    }
}
