﻿using Models.ArcModel;
using Models.DBModel;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.ServiceDal
{
    public class BuildingDal : Imorphy
    {
        protected MContext Mcontext { get; set; }
        public BuildingDal(MContext mContext)
        {
            Mcontext = mContext;
        }

        public List<BuildingModel> GetBuildings()
        {
            return Mcontext.Buildings.Where(p => p.IsActive).ToList();
        }

        public BuildingModel SaveBuilding(BuildingModel model)
        {
            model.IsActive = true;
            Mcontext.Buildings.Add(model);
            Mcontext.SaveChanges();
            Mcontext.activityService.SaveActivity(new ActivityModel() { action = "insert building", tableName = "Buildings", tableId = model.id });
            return model;
        }
        public BuildingModel DeleteBuilding(BuildingModel model)
        {
            var tmp = Mcontext.Buildings.FirstOrDefault(p => p.id == model.id);
            if (tmp != null)
            {
                tmp.IsActive = false;
                Mcontext.SaveChanges();
                Mcontext.activityService.SaveActivity(new ActivityModel() { action = "delete building", tableName = "Buildings", tableId = model.id });
            }
            return model;
        }
    }
}
