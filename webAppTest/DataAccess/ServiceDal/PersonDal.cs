﻿using Models.ArcModel;
using Models.DBModel;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.ServiceDal
{
    public class PersonDal : Imorphy
    {
        protected MContext Mcontext { get; set; }
        public PersonDal(MContext mContext)
        {
            Mcontext = mContext;
        }

        public List<PersonModel> GetPerosns()
        {
            return Mcontext.Persons.ToList();
        }
    }
}
