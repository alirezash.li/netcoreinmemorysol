﻿using Microsoft.EntityFrameworkCore;
using Models.ArcModel;
using Models.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.ServiceDal
{
    public class PersonBuildingDal : Imorphy
    {
        protected MContext Mcontext { get; set; }
        public PersonBuildingDal(MContext mContext)
        {
            Mcontext = mContext;
        }

        public List<PersonBuildingModel> GetPersonBuildings()
        {
            return Mcontext.PersonBuildings.Include(p => p.person).Include(p => p.building).Where(p => p.isActive).ToList();
        }

        public PersonBuildingModel SavePersonBuildings(PersonBuildingModel model)
        {
            model.isActive = true;
            model.createDate = DateTime.Now;
            Mcontext.PersonBuildings.Add(model);
            Mcontext.SaveChanges();
            Mcontext.activityService.SaveActivity(new ActivityModel() { action = "SavePersonBuildings", tableName = "SavePersonBuilding", tableId = model.id });
            return model;
        }
        public PersonBuildingModel DeletePersonBuildings(PersonBuildingModel model)
        {
            model.isActive = true;
            var tmp = Mcontext.PersonBuildings.FirstOrDefault(p => p.id == model.id);

            if (tmp != null)
            {
                tmp.isActive = false;
                Mcontext.SaveChanges();
                Mcontext.activityService.SaveActivity(new ActivityModel() { action = "SavePersonBuildings", tableName = "SavePersonBuilding", tableId = model.id });
            }
            return model;
        }
    }
}
