﻿using Models.ArcModel;
using Models.DBModel;
using System;

namespace DataAccess.ServiceDal
{
    public class ActivityDal : Imorphy
    {
        protected MContext Mcontext { get; set; }
        public ActivityDal(MContext mContext)
        {
            Mcontext = mContext;
        }

        public ActivityModel SaveActivity(ActivityModel model)
        {
            model.createDate = DateTime.Now;
            Mcontext.Activities.Add(model);
            Mcontext.SaveChanges();
            return model;
        }

    }
}
