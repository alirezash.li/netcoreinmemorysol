﻿using DataAccess.ServiceDal;
using Microsoft.EntityFrameworkCore;
using Models.DBModel;
using System.Collections.Generic;
using System.Linq;
namespace DataAccess
{
    public class MContext : DbContext
    {

        public BuildingDal bulidingService { get; set; }
        public PersonDal personService { get; set; }
        public ActivityDal activityService { get; set; }
        public PersonBuildingDal PersonBuildingService { get; set; }

        public DbSet<BuildingModel> Buildings { get; set; }
        public DbSet<PersonModel> Persons { get; set; }
        public DbSet<PersonBuildingModel> PersonBuildings { get; set; }
        public DbSet<ActivityModel> Activities { get; set; }

        public List<BuildingModel> GetBuildings()
        {
            return Buildings.ToList();
        }

        public MContext(DbContextOptions<MContext> setting) : base(setting)


        {

            bulidingService = new BuildingDal(this);
            personService = new PersonDal(this);
            activityService = new ActivityDal(this);
            PersonBuildingService = new PersonBuildingDal(this);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BuildingModel>().HasKey(p => p.id);
            modelBuilder.Entity<PersonBuildingModel>().HasKey(p => p.id);
            modelBuilder.Entity<PersonModel>().HasKey(p => p.id);


            //modelBuilder.Entity<PersonModel>().HasMany(p => p.PersonBuildingModels).WithOne().HasForeignKey(p=>p.personid);
            //modelBuilder.Entity<BuildingModel>().HasMany(p =>p.PersonBuildingModels).WithOne().HasForeignKey(p=>p.buildingId);
            modelBuilder.Entity<PersonBuildingModel>().HasOne(p => p.person).WithMany().HasForeignKey(p => p.personid);
            modelBuilder.Entity<PersonBuildingModel>().HasOne(p => p.building).WithMany().HasForeignKey(p => p.buildingId);



        }


        public void InMemoryInit(List<BuildingModel> builingList, List<PersonModel> persons)
        {


            Buildings.AddRange(builingList);
            Persons.AddRange(persons);
            SaveChanges();
        }


    }
}
